# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# IF age >= 18 or has_consent_form
    # return Can skydive
# return Cannot Skydive. Too young. They need permission from gardian.
def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form:
        return 'Can Skydive'
    return 'Cannot Skydive. Too young. They need permission from gardian.'

print('can dive:',  can_skydive(12, True))
print('can dive:', can_skydive(18, False))
print('can dive:', can_skydive(22, False))
print('Cannot --->')
print('NOT dive:', can_skydive(17, False))
