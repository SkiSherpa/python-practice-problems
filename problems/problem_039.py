# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    dic = {}
    for key in dictionary:
        new_key = dictionary[key]
        new_val = key
        dic[new_key] = new_val
    return dic

print({"value", "key"}, reverse_dictionary({"key": "value"}))
print({1: "one", 2: "two", 3: "three"}, reverse_dictionary({"one": 1, "two": 2, "three": 3}))
