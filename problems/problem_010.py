# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# IP: single number
# OP: 'fizz' if IP num is divisible by 3, returns the Ip num if not
# edge: should be able to work with negative numbers -> % works with neg in py, only JS has the issue
        # IF zero is IP, should return 'fizz' imo
        # floats should return the float
# c: ignoring imaginary nums and Non-num inputs
def is_divisible_by_3(number):
    if number == 0:
        return 'fizz'
    if isinstance(number, float):
        return number

    if number % 3 == 0:
        return 'fizz'
    return number




print('fizz', is_divisible_by_3(3))
print('fizz', is_divisible_by_3(9))
print('fizz', is_divisible_by_3(-9))
print('fizz', is_divisible_by_3(0))
print('10', is_divisible_by_3(10))
print('-10', is_divisible_by_3(-10))
print('3.3', is_divisible_by_3(3.3))
