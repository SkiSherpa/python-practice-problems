# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# IP: string
# OP: Bool - True IF IP is a palindrone
# Constraints: assume all inputs are valid
# Edge: empty strings could be and issue, will return True if a problem, cause 0 == 0

# make a empty 'word_list'
# make empty 'reverse_word_list'
# make 'word' all lowercase
# loop thro 'word'
    # place each letter to BACK into a 'word_list'
    # place each letter to FRONT into a 'reversed_word_list'
# loop thro 'word_list'
    # IF current letters in word_list and 'reversed_word_list' do NOT match
        # return False
# return True

def is_palindrome(word):
    word = word.lower()
    # copy_word = copy.copy(word)
    # print(copy_word)
    word_list = []
    reverse_word_list = []
    for letter in word:
        word_list.append(letter)
        reverse_word_list.insert(0, letter)
    for i in range(len(word_list)):
        if word_list[i] != reverse_word_list[i]:
            return False
    return True


print('True:', is_palindrome('racecar'))
print('False:', is_palindrome('abc'))
print('True:', is_palindrome('Racecar'))
