# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one
import random
def specific_random():
    viable_nums = []
    for num in range(10, 501):
        if num % 5 == 0 and num % 7 == 0:
            viable_nums.append(num)
    random_i = random.randint(0, len(viable_nums) - 1)
    random_num = viable_nums[random_i]
    return random_num

ans = specific_random()
print(ans, 'ans/5 = ', ans/5, 'ans/7 = ', ans/7)
