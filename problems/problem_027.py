# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max = values[0]
    for num in values:
        if num > max:
            max = num
    return max


print(100, max_in_list([10, 29, 100, 90, 60]))
print(1, max_in_list([0, -1, -10, 1, -9]))
print(-9, max_in_list([-19, -9, -27]))
