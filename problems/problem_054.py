# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError

def check_input(val):
    if val == "raise":
        raise ValueError('input value is "raise"')
        return ''
    return val

print(check_input('hello'))
# print(check_input('raise'))
print(check_input(1))
print(check_input(False))

# !!!! Raising an error stops the program.
# IP 1 & False never ran when the argu 'raise' was run
