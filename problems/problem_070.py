# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")
#
#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

# Class Book
  # initialize state method
    # set state for:
      # author name
      # title of book
class Book():
    def __init__(self, author, title):
        self.author = author
        self.title = title
  # get method for retrieving authors name
    def get_author(self):
        return "Author: " + self.author
  # get method for retrieving books title
    def get_title(self):
        return "Title: " + self.title

book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())  # prints "Author: Natalie Zina Walschots"
print(book.get_title())   # prints "Title: Hench"
