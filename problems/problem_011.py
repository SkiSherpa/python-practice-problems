# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# IP: single number
# OP: 'buzz' if IP num is divisible by 5, returns the Ip num if not
# edge: should be able to work with negative numbers ->
        # IF zero is IP, should return 'buzz' imo
        # floats should return the float
# c: ignoring imaginary nums and Non-num inputs
def is_divisible_by_5(number):
    if number == 0:
        return 'buzz'
    if isinstance(number, float):
        return number

    if number % 5 == 0:
        return 'buzz'
    return number

print('buzz', is_divisible_by_5(5))
print('buzz', is_divisible_by_5(10))
print('buzz', is_divisible_by_5(-10))
print('buzz', is_divisible_by_5(0))
print('9', is_divisible_by_5(9))
print('-9', is_divisible_by_5(-9))
print('3.3', is_divisible_by_5(3.3))
