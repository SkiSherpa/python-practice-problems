# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny AND it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# IP: 2 bools
# OP: a list containing gear for the day.
# C: n/a - assume valid IP
# E: n/a

# IF workday == True
    # add laptop to list
    # IF is_sunny = False
        # add umbrella to list
# ELSE add surfboard to list


def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_workday:
        gear.append('laptop')
        if not is_sunny:
            gear.append('umbrella')
    else:
        gear.append('surfboard')
    return gear

print(['laptop', 'umbrella'], '--', gear_for_day(True, False))
print(['laptop'], '--', gear_for_day(True, True))
print(['surfboard'], '--', gear_for_day(False, False))
print(['surfboard'], '--', gear_for_day(False, True))
