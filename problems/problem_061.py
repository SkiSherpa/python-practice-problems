# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
input =   [1, 1, 1, 1]
#       returns: [1]
input2 =   [1, 2, 2, 1]
#       returns: [1, 2]
input3 =   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

def remove_duplicates(values):
    uniq_nums = []
    for num in values:
        if num not in uniq_nums:
            uniq_nums.append(num)
    return uniq_nums

print(remove_duplicates(input))
print(remove_duplicates(input2))
print(remove_duplicates(input3))
