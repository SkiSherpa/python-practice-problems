# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

# rect_x, rect_y, rect_width, rect_height are SEPERATE than x & y
# seeing if (x, y) are in the given rectange

# C: Assume on the edge counts, using Tennis rules, IF on the line its in
#       Assuming valid number inputs
# use same flags as before
def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    x_in_bounds = False
    y_in_bounds = False
    if x >= rect_x and x <= (rect_x + rect_width):
        x_in_bounds = True
    if y >= rect_y and y <= (rect_y + rect_height):
        y_in_bounds = True
    if x_in_bounds and y_in_bounds:
        return True
    return False

print('True', is_inside_bounds(1, 1, 0, 0, 4, 4))
print('False', is_inside_bounds(-1, -1, 0, 0, 4, 4))
