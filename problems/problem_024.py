# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you


def calculate_average(values):
    if len(values) == 0:
        return None
    sum = 0
    for num in values:
        sum += num
    avg = sum / len(values)
    return avg

print('2.5', calculate_average([1,2,3,4]))
print('None', calculate_average([]))
print('-2.5', calculate_average([-1,-2,-3,-4]))
print('4.0', calculate_average([8,2,2,4]))
