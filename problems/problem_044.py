# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    results = []
    for i in range(len(key_list)):
        if key_list[i] in dictionary:
            results.append(dictionary[key_list[i]])
    return results

print(["Noor", 29], translate(["name", "age"], {"name": "Noor", "age": 29}))
print([29, 29, 29], translate(["age", "age", "age"], {"name": "Noor", "age": 29}))
print([None, 29], translate(["eye color", "age"], {"name": "Noor", "age": 29}))
