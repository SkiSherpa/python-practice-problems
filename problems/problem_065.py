# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
input =  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
input2 =  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
input3 =  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(numbers):
    biggest = 0
    i = 0
    while (i < len(numbers) - 1):
        current_diff = abs(numbers[i] - numbers[i+1])
        if current_diff > biggest:
            biggest = current_diff
        i += 1
    return biggest

print(2, biggest_gap(input))
print(20, biggest_gap(input2))
print(97, biggest_gap(input3))
