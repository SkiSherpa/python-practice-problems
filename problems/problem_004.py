# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# IF val1 >= val2
    # IF val1 >= 3
        # return 1
# IF 2 >= 1
    # IF 2 >= 3
        # return 2
# return 3

def max_of_three(value1, value2, value3):
    if value1 >= value2:
        if value1 >= value3:
            return value1
    if value2 >= value1:
        if value2 >= value3:
            return value2
    return value3

print('No vals are equal:')
print('val1 = 3', max_of_three(3, 2, 1))
print('val2 = 3', max_of_three(1, 3, 2))
print('val3 = 3', max_of_three(1, 2, 3))
print('Some are equal:')
print('val1 & 2 = 3', max_of_three(3, 3, 1))
print('val2 & 3 = 3', max_of_three(1, 3, 3))
print('val3 & 1 = 3', max_of_three(3, 2, 3))
