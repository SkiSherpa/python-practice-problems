# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1 + 2*2 + 3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def sum_of_squares(values):
    sum = 0
    for num in values:
        square = num ** 2
        sum += square
    return sum

print(None, sum_of_squares([]))
print(14, sum_of_squares([1, 2, 3]))
print(2, sum_of_squares([-1, 0, 1]))
