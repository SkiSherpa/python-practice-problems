# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
input =   [1, 2, 3, 4]
#       returns: [1, 3]
input2 =   [2, 4, 6, 8]
#       returns: []
input3 =   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

def only_odds(numbers):
    odds = []
    for num in numbers:
        if num % 2 == 1:
            odds.append(num)
    return odds


print(only_odds(input))
print(only_odds(input2))
print(only_odds(input3))
