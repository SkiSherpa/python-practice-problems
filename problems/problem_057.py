# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

# 1+0   1+1   1+2
# 2+0   2+1   2+2
def sum_fraction_sequence(n):
    sum = 0
    numbers = range(n)
    for i in numbers:
        sum += (1+i) / (2+i)
    return sum


ip3 = (1/2) + (2/3) + (3/4)
ip2 = (1/2) + (2/3)
print(ip3, sum_fraction_sequence(3))
print(ip2, sum_fraction_sequence(2))
print(sum_fraction_sequence(1))
