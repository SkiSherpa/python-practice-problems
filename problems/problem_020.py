# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.
# attendees < members, IF all members present -> attednees/members = 1

# IP: 2 lists of people
# OP: Bool - True if numb of people (attend.) is greater OR equal than 50% of the total num of people (member)
# C: member_list will always be larger than attendess list
# E: N/a - assuming valid inputs
# IF len of att / len of member >= 0.5
    # return True
# return False
def has_quorum(attendees_list, members_list):
    if len(attendees_list) / len(members_list) >= 0.5:
        return True
    return False

print('True', has_quorum(['a', 'b', 'c'], ['a', 'b', 'c', 'd']))
print('True', has_quorum(['a', 'b'], ['a', 'b', 'c', 'd']))
print('False', has_quorum([], ['a', 'b', 'c', 'd']))
print('False', has_quorum(['a'], ['a', 'b', 'c', 'd']))
