# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

# IP: 2 nums
# OP: bool - True if IPs are between 0 & 10, inclusive
# C: Assuming valid IP
# E: n/a

# make a x_in_bounds and y_in_bounds var set to False
# IF x and y being in bounds
    # set flags to True if in bounds
# compare flags

def is_inside_bounds(x, y):
    x_in_bounds = False
    y_in_bounds = False
    if x >= 0 and x <= 10:
        x_in_bounds = True
    if y >= 0 and y <= 10:
        y_in_bounds = True
    if x_in_bounds and y_in_bounds:
        return True
    return False

print('True', is_inside_bounds(2, 6))
print('True', is_inside_bounds(0, 6))
print('True', is_inside_bounds(2, 10))
print('False', is_inside_bounds(-2, 6))
print('False', is_inside_bounds(2, 16))
