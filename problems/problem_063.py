# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
inputs =  "import"
result =  "jnqpsu"
inputs2 =  "ABBA"
result2 =  "BCCB"

inputs3 =  "Kala"
result3 =  "Lbmb"

inputs4 =  "zap"
result4 =  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
# ord : letter -> "number"
def shift_letters(word):
    new_word = ''
    for char in word:
        if char == 'z':
            new_word += 'a'
        elif char == 'Z':
            new_word += 'A'
        else:
            letter_num = ord(char)
            letter_num += 1
            new_letter = chr(letter_num)
            new_word += new_letter
    return new_word


# print(result, shift_letters(inputs))
# print(result2, shift_letters(inputs2))
# print(result3, shift_letters(inputs3))
print(result4, shift_letters(inputs4))
