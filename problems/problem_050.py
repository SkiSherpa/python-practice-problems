# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
input1 = [1, 2, 3, 4]
# result: [1, 2], [3, 4]
input2 = [1, 2, 3]
# result: [1, 2], [3]

def halve_the_list(list):
    half_way_i = len(list) // 2

    first_half = list[:half_way_i]
    second_half = list[half_way_i:]
    return_list = first_half, second_half
    # return first_half, second_half
    return return_list

input1 = [1, 2, 3, 4]
print(halve_the_list(input1))
# print([1, 2], [3], halve_the_list(input2))
# The return of mult vals, returns them as a tupil
# half_the_list(input1) returns ([1,2], [3,4])
# to not have it as a tupil you can do below, set the fn returning the tupil var that is the num of vals that is being returned
# first, second = halve_the_list(input1)
# print(first, second)

#
def test(list):
    testing = halve_the_list(list)
    return testing

# print(test(input1))
# test(input2)
