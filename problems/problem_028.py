# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# IP: a str
# OP: str - with duplicate letters removed
# C: Assume valid IP. All inputs are lowercase
# E: IF str is empty return None
def remove_duplicate_letters(s):
    if len(s) == 0:
        return None

    single_letters = []
    # loop through "s"
        # IF current letter NOT IN S
            # add current letter to single_letter
    for current_letter in s:
        if current_letter not in single_letters:
            single_letters.append(current_letter)

    no_dup_str = "".join(single_letters)
    return no_dup_str

print(None, remove_duplicate_letters(""))
print("abc ", remove_duplicate_letters("abc"))
print("abc ", remove_duplicate_letters("abcabc"))
print("abc ", remove_duplicate_letters("abccba"))
print("abcd", remove_duplicate_letters("abccbad"))
