# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

# .is -  alpha, digit, upper, lower - return true if all in string match
# IP: a string
# OP: boo - True if passes all criteria

def check_password(password):
    if len(password) < 6 or len(password) >= 12:
        return False
    if password.isalpha():
        return False
    if password.isdigit():
        return False
    if password.islower():
        return False
    if password.isupper():
        return False
    is_special_char = False
    for char in password:
        if char == '$' or char == '!' or char == '@':
            is_special_char = True

    if is_special_char:
        return True

    return False

print(True, '  ', check_password('P@ssword1'))
print(False, '  ', check_password('password'))
print(False, '  ', check_password('PASSWORD'))
print(False, '  ', check_password('p$sswordp@ssword'))
print(False, '  ', check_password('12345678'))
print(False, '  ', check_password('p@S1'))
