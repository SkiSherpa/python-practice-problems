# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return 'F'
    sum = 0
    for cur_grade in values:
        sum += cur_grade

    avg = sum / len(values)
    if avg >= 90:
        return 'A'
    if avg >= 80:
        return 'B'
    if avg >= 70:
        return 'C'
    if avg >= 60:
        return 'D'
    return 'F'

print('F', calculate_grade([]))
print('A', calculate_grade([90, 90, 90]))
print('A', calculate_grade([90, 91, 100]))
print('B', calculate_grade([90, 80]))
print('C', calculate_grade([69, 72, 82]), (69+72+82)/3)
print('D', calculate_grade([60]))
print('F', calculate_grade([90, 0, 10, 20, 30]), (90+0+10+20+30)/5)
