# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# IP: A list of ingredients
# OP: Boo - True if ingre includes flour, eggs, and oil
# C: assume valid IP
# E: list is empty, or below 3 items

# make a count var set to 0
# loop thro list
    # IF item == flour
        # make count++
    # If item == eggs
        # make count++
    # If item == oil
        # make count++
# IF count >= 3
    # return True
# return False
def can_make_pasta(ingredients):
    if len(ingredients) < 3:
        return False
    has_enough_ingredients = 0
    for item in ingredients:
        if item == 'flour':
            has_enough_ingredients += 1
        if item == 'eggs':
            has_enough_ingredients += 1
        if item == 'oil':
            has_enough_ingredients += 1
    if has_enough_ingredients >= 3:
        return True
    return False


print('True', can_make_pasta(['flour', 'eggs', 'pepper', 'oil', 'salt']))
print('False', can_make_pasta(['eggs', 'pepper', 'oil', 'salt']))
print('True', can_make_pasta(['flour', 'eggs', 'pepper', 'oil', 'salt', 'flour']))
