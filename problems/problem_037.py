# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# create a list - to hold str_num and padding
# add padding char one at a time to front
# join and return

def pad_left(number, length, pad):
    num_length = len(str(number))
    if num_length >= length:
        return number
    str_num = str(number)
    will_join = [str_num]
    iterations = length - len(str_num)
    for i in range(iterations):
        will_join.insert(0, pad)
    padded = ''.join(will_join)
    return padded

print('**10 ', pad_left(10, 4, '*'))
print('1000', pad_left(1000, 3, '0'))
