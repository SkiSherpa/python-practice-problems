# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# E: IF zero is IP, should return 'buzz' imo
        # floats should return the float
# C: ignoring imaginary nums and Non-num inputs

#
def fizzbuzz(number):
    if number == 0:
        return 'fizzbuzz'
    if isinstance(number, float):
        return number

    if number % 3 == 0 and number % 5 == 0:
        return 'fizzbuzz'
    if number % 3 == 0:
        return 'fizz'
    if number % 5 == 0:
        return 'buzz'
    return number

print('fizzbuzz', fizzbuzz(15))
print('fizzbuzz', fizzbuzz(-15))
print('fizzbuzz', fizzbuzz(0))
print('5.5', fizzbuzz(5.5))
print('fizz', fizzbuzz(12))
print('fizz', fizzbuzz(-9))
print('buzz', fizzbuzz(10))
print('buzz', fizzbuzz(-10))
